# HabrTopArticleParser

### What is the project about
Parser analyses best articles past 24 hours from [Хабр](https://habr.com/) and adds them
into *MySQL* table which is created based on the current date.<br/>
Table name **t_672020** means that the table was generated on 6th of July, 2020.