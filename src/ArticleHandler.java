import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ArticleHandler {
    protected Article article;
    protected List<String> articleTime;
    protected List<String> articleTitle;
    protected List<String> articleUrl;

    public ArticleHandler() {
        this.article = new Article();
        this.articleTime = new LinkedList<>();
        this.articleTitle = new LinkedList<>();
        this.articleUrl = new LinkedList<>();
    }

    public void handle() throws IOException {
        Document document = Jsoup.connect("https://habr.com/ru/top/").get();
        Elements header = document.getElementsByAttributeValueMatching("id", "post_\\d{6}");

        header.forEach(headerElem -> {
            Element articleElem = headerElem.child(0);
            article.time = articleElem.getElementsByAttributeValue("class", "post__time").text();
            if (!article.time.isBlank())
                articleTime.add(article.time);
            article.title = articleElem.getElementsByAttributeValue("class", "post__title_link").text();
            if (!article.title.isBlank())
                articleTitle.add(article.title);
            article.url = articleElem.getElementsByAttributeValue("class", "post__title").select("a").attr("href");
            if (!article.url.isBlank())
                articleUrl.add(article.url);
        });
    }
}