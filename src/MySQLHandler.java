import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLHandler implements Data {
    ArticleHandler article;
    private final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String URL = "jdbc:mysql://localhost:3306/htmlparser?useUnicode=true&serverTimezone=UTC";
    private final String NAME = "root";
    private final String PASSWORD = "enuvan16";

    public MySQLHandler(ArticleHandler article) {
        this.article = article;
    }

    @Override
    public void createTable() {
        String createTableQuery = "CREATE TABLE IF NOT EXISTS t_" + Date.getDate() + " (\n" +
                "publication_time LONGTEXT,\n" +
                "title LONGTEXT,\n" +
                "url LONGTEXT\n" +
                ");";
        try {
            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(URL, NAME, PASSWORD);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(createTableQuery);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getValues() {
        final int postsToHandle = 15;
        StringBuilder values = new StringBuilder();
        for (var post = 0; post < postsToHandle; post += 1) {
            values.append("(\"");
            values.append(article.articleTime.get(post));
            values.append("\", \"");
            values.append(article.articleTitle.get(post));
            values.append("\", \"");
            values.append(article.articleUrl.get(post));
            values.append("\"),");
        }
        values.deleteCharAt(values.length() - 1);

        return values.toString();
    }

    @Override
    public void fillTable() {
        String insertIntoTableQuery = "INSERT INTO t_" + Date.getDate() +
                "(publication_time, title, url)" +
                "VALUES" + getValues() + ";";
        try {
            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(URL, NAME, PASSWORD);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(insertIntoTableQuery);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String handle() {
        createTable();
        fillTable();
        return "New table was successfully created";
    }
}