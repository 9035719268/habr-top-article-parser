import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        ArticleHandler articleHandler = new ArticleHandler();
        articleHandler.handle();
        Data dataHandler = new MySQLHandler(articleHandler);
        System.out.println(dataHandler.handle());
    }
}