import java.util.Calendar;

public class Date {
    public static String getDate() {
        Calendar calendar = Calendar.getInstance();
        return String.valueOf(calendar.get(Calendar.DATE)) +
                String.valueOf(calendar.get(Calendar.MONTH)) +
                String.valueOf(calendar.get(Calendar.YEAR));
    }
}