public interface Data {
    void createTable();
    String getValues();
    void fillTable();
    String handle();
}